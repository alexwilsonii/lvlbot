const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');
const mysql = require('mysql');
const moment = require('moment');
const AsciiTable = require('ascii-table')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('pacing')
        .setDescription(`Determine the current pace of an LvL (max points possible, points to clinch, etc)`)
        .addIntegerOption(option => option.setName('numplayers').setDescription(`The number of players in LvL`).setRequired(true))
        .addIntegerOption(option => option.setName('numdrivestaken').setDescription(`The number of drives completed`).setRequired(true))
        .addIntegerOption(option => option.setName('score').setDescription(`Current score`).setRequired(true))
        .addIntegerOption(option => option.setName('opponentdrivestaken').setDescription(`The number of opponent drives completed`).setRequired(true))
        .addIntegerOption(option => option.setName('opponentscore').setDescription(`Opponent's current score`).setRequired(true)),
    async execute(interaction) {

	const roleReq = 'Rockstar Moderator';

	if (!interaction.member.roles.cache.some(role => role.name.toUpperCase() === roleReq.toUpperCase())) {
	    await interaction.reply({ content: 'You do not have permission to use this command; it is limited to the ' + roleReq + ' only.', ephemeral: true});
	    return
	}

        const userDiscordId = interaction.user.id;
        const numPlayers = interaction.options.getInteger('numplayers');
        const numDrives = interaction.options.getInteger('numdrivestaken');
        const score = interaction.options.getInteger('score');
        const oppDrives = interaction.options.getInteger('opponentdrivestaken');
        const oppScore = interaction.options.getInteger('opponentscore');

	const currPpd 		= parseFloat((score / numDrives).toFixed(1));
	const oppCurrPpd 	= parseFloat((oppScore / oppDrives).toFixed(1));

	const drivesLeft 	= (numPlayers * 3) - numDrives;
	const oppDrivesLeft 	= (numPlayers * 3) - oppDrives;

	const maxScore 		= (8 * drivesLeft) + score;
	const oppMaxScore 	= (8 * oppDrivesLeft) + oppScore;

	var ptsToClinch		= oppMaxScore + 1 - score;
	var oppPtsToClinch	= maxScore + 1 - oppScore;

	if (ptsToClinch < 0) { ptsToClinch = "0" }
	if (oppPtsToClinch < 0) { oppPtsToClinch = "0" }

	var table = new AsciiTable()
	table
	  .setHeading('Stats', 'Us', 'Opponent')
	  .addRow('Score', score, oppScore)
	  .addRow('Drives Taken', numDrives, oppDrives)
	  .addRow('Drives To Play', drivesLeft, oppDrivesLeft)
	  .addRow('Current PPD', currPpd, oppCurrPpd)
	  .addRow('Max Pts', maxScore, oppMaxScore)
	  .addRow('Pts to Clinch', ptsToClinch, oppPtsToClinch);

        const reply = new MessageEmbed();
        reply.setColor('#0099ff')
             .setTitle('Pacing')
             .setDescription('```' + table.toString() + '```');

	if (ptsToClinch == "0") {
	    reply.setTitle('Pacing -- WIN!');
	} else if (oppPtsToClinch == 0) {
	    reply.setTitle('Pacing -- LOSS');
	}

        await interaction.reply({embeds: [reply]});
    },
};

