const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');
const config = require('../config.json');
const mysql = require('mysql');
const moment = require('moment');

const pool = mysql.createPool({
    connectionLimit : 100,
    host     : 'localhost',
    user     : config.dbUser,
    password : config.dbPass,
    database : config.dbName,
    debug    :  false,
});


module.exports = {
    data: new SlashCommandBuilder()
        .setName('setopponent')
        .setDescription(`Set today's LvL opponent`)
        .addStringOption(option => option.setName('opponentname').setDescription(`The opponent's league name`).setRequired(true))
        .addIntegerOption(option => option.setName('opponentrank').setDescription(`The opponent's rank`).setRequired(true))
        .addIntegerOption(option => option.setName('ourrank').setDescription(`Chill Town's rank`).setRequired(true)),
    async execute(interaction) {

	if (!interaction.member.roles.cache.some(role => role.name.toUpperCase() === 'Chill Town Admin'.toUpperCase())) {
	    await interaction.reply({ content: 'You do not have permission to use this command; it is limited to admins only.', ephemeral: true});
	    return
	}

        const userDiscordId = interaction.user.id;
        const oppName = interaction.options.getString('opponentname');
        const oppRank = interaction.options.getInteger('opponentrank');
        const ourRank = interaction.options.getInteger('ourrank');

        const reply = new MessageEmbed();
        const newOppId = await addOpponent(userDiscordId, oppName, oppRank, ourRank);
        if (newOppId > 0) {
            reply.setColor('#0099ff')
                .setTitle('Opponent Set')
                .setDescription(`Playing ${oppName} today. They are ranked #${oppRank} and we're ranked #${ourRank}.\nGood luck to all!`);
        } else {
            reply.setColor('#ff0000')
                .setTitle('Error')
                .setDescription('An error occurred while trying to set the opponent');
        }
        await interaction.reply({embeds: [ reply ]});
    },
};

function addOpponent(addedById, oppName, oppRank, ourRank) {
    const insertQuery = 'INSERT INTO opponents (name, date, rank, our_rank, score, our_score, created_by) VALUES (?, ?, ?, ?, ?, ?, ?)';
    const date = moment().format('YYYY-MM-DD');
    const query = mysql.format(insertQuery, [oppName, date, oppRank, ourRank, 0, 0, addedById]);
    return new Promise((resolve, reject) => {
        pool.query(query, (err, response) => {
            if (err) {
                return reject(err);
            }
            return resolve(response.insertId);
        });
    });
}
